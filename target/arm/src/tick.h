/**
 *  tick.h : allows the accurate measurement of elapsed time
 *  
 *  Author: Mark Sabido
 *  
 *  This module works independently of clock frequency or reload value as it uses the intrinsic global timer variables in it's calculations
 *  
 *  Maxima and minima:
 *  Maximum elapsed time: (0xFFFFFFFFFFFFFFFF/168000000)/(3600*7*24*52) = 3491 years
 */
#ifndef TICK_H
#define TICK_H

#include <stdint.h>
//#include "cfg.h"

/**
 *  TICK_TASK_OBJ : Object that represents a tick task
 */
typedef struct
{
	uint8_t				execute;														// non-zero to execute the task at all
	uint32_t			it_divisor;														// execute every n timer cycles

	void 				*callback_instance;												// Instance to pass in to the callback structure
	void				(*tick_task_cb)(void *instance, const uint64_t *elapsed_us);	// Callback, called every cycle
	uint32_t			it_divisor_countdown;											// counts down from it_divisor
	volatile uint64_t	*start_ticks;													// Represents the number of ticks to measure elapsed time against
}
TICK_TASK_OBJ;

/**
 *  TICK_TASK_INIT - structure to initialise a tick task
 *  Only certain frequencies are supported
 *  frequency	count			seconds			minutes
 *  20000		1				0.00005			8.33333333333333E-007
 *  10000		2				0.0001			1.66666666666667E-006
 *  5000		4				0.0002			3.33333333333333E-006
 *  4000		5				0.00025			4.16666666666667E-006
 *  2000		10				0.0005			8.33333333333333E-006
 *  1000		20				0.001			1.66666666666667E-005
 *  500			40				0.002			3.33333333333333E-005
 *  400			50				0.0025			4.16666666666667E-005
 *  200			100				0.005			8.33333333333333E-005
 *  100			200				0.01			0.0001666667
 *  50			400				0.02			0.0003333333
 *  40			500				0.025			0.0004166667
 *  20			1000			0.05			0.0008333333
 *  10			2000			0.1				0.0016666667
 *  5			4000			0.2				0.0033333333
 *  4			5000			0.25			0.0041666667
 *  2			10000			0.5				0.0083333333
 *  1			20000			1				0.0166666667
 *  0.5			40000			2				0.0333333333
 *  0.4			50000			2.5				0.0416666667
 *  0.2			100000			5				0.0833333333
 *  0.1			200000			10				0.1666666667
 *  0.05		400000			20				0.3333333333
 *  0.04		500000			25				0.4166666667
 *  0.02		1000000			50				0.8333333333
 *  0.01		2000000			100				1.6666666667
 *  0.005		4000000			200				3.3333333333
 *  0.004		5000000			250				4.1666666667
 *  0.002		10000000		500				8.3333333333
 *  0.001		20000000		1000			16.6666666667
 *  0.0005		40000000		2000			33.3333333333
 *  0.0004		50000000		2500			41.6666666667
 *  0.0002		100000000		5000			83.3333333333
 *  0.0001		200000000		10000			166.6666666667
 *  0.00005		400000000		20000			333.3333333333
 *  
 *  Max uint32:	4294967296
 *  
 */
typedef struct
{
	uint32_t			it_divisor;		// performs the task every x ticks
	void 				*callback_instance;			// Instance to pass in to the callback structure
	void				(*tick_task_cb)(void *instance, const uint64_t *elapsed_us);
	volatile uint64_t	*start_ticks;													// Represents the number of ticks to measure elapsed time against
}
TICK_TASK_INIT;

/**
 *  TICK_init - initialises the system clock
 *  The system counts at the oscillator clock rate. The clock rate is defined in SystemCoreClock and can be used for calculations
 */
uint8_t TICK_init(uint32_t reload);

/**
 *  TICK_get_cur - returns the current counter value, to be used to calculate elapsed time
 */
uint64_t TICK_get_cur(void);

/**
 *  TICK_elapsed_ns - returns the elapsed time in nano seconds. Maximum return value will be 4.294967295 seconds
 */
uint64_t TICK_elapsed_ns(uint64_t	start);

/**
 *  TICK_elapsed_us - returns the elapsed time micro seconds
 */
uint64_t TICK_elapsed_us(uint64_t	start);

/**
 *  TICK_timespan_us - calculates the length of time between two tick values
 */
uint64_t TICK_timespan_us(uint64_t start, uint64_t end);

/**
 *  TICK_elapsed_ms - returns the elapsed time milliseconds
 */
uint64_t TICK_elapsed_ms(uint64_t	start);

/**
 *  TICK_add_task
 */
uint8_t TICK_add_task(TICK_TASK_INIT *init, TICK_TASK_OBJ *obj, volatile uint64_t *start_ticks);

/**
 *  TICK_start_task
 */
void TICK_start_task(TICK_TASK_OBJ *task_obj);

/**
 *  TICK_stop_task
 */
void TICK_stop_task(TICK_TASK_OBJ *task_obj);

/**
 * TICK_set_it_divisor
 */
void TICK_set_it_divisor(TICK_TASK_OBJ *task_obj, uint32_t it_divisor);


///////////////////////////////////////////////////////////////////////////////////
//
// Debugging routines
//
#ifdef DEBUG_TICK
typedef struct
{
	uint64_t systick_high;
	uint64_t val;
	uint64_t load;
} TICK_DATA;

/**
 *  TICK_get_cur_ex - records timer information for debugging purposes
 */
uint64_t TICK_get_cur_ex(TICK_DATA *td);

/**
 *  TICK_elapsed_us_ex - records timer information for debugging purposes
 */
uint64_t
TICK_elapsed_us_ex(uint64_t start, TICK_DATA *start_td);

#define TIME_START uint64_t time; time = TICK_get_cur();
#define TIME_STOP(STR,US)  if (TICK_elapsed_us(time) > US) DBG_printf(D0,"%s:%lu\r\n",STR,(unsigned long)TICK_elapsed_us(time));

#endif // DEBUG_TICK

#define EVERY_MS(X)									\
			char execute = 0;						\
			{										\
			static uint64_t ticks = 0;				\
			if (TICK_elapsed_ms(ticks) > X)			\
			{										\
				ticks = TICK_get_cur();				\
				execute = 1;						\
			}										\
			}										\
			if (execute)


#endif // TICK_H
