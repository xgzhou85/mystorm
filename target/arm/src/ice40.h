#ifndef __ICE40__
#define __ICE40__

#include "stm32f10x.h"
#include "tick.h"

#define LED GPIO_Pin_15
#define LEDP GPIOA

enum programstates {OK,TIMEOUT,ICE_ERROR};

void initialise_ice40(void);
uint8_t ice40_reset(void);
uint8_t ice40_program(uint8_t *bitimg, uint32_t len);



#endif
